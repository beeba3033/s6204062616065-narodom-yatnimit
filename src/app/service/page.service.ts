import { Injectable } from '@angular/core';
import {Flight} from "../component/form/flight";
import {MockData} from "../share/mock-data";

@Injectable({
  providedIn: 'root'
})
export class PageService {
  flight:Flight[] = [];
  constructor() {
    this.flight = MockData.mfilght;
  }
  getFlight():Flight[]{
    return this.flight ;
  }
  addFlight(_flight:Flight):void{
    this.flight.push(_flight);
  }
  log(msg:any){
    console.log(new Date() + ": " + JSON.stringify(msg));
  }
}
