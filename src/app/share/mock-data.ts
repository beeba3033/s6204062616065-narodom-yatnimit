import {Flight} from "../component/form/flight";
import {NgbDate} from '@ng-bootstrap/ng-bootstrap';
export class MockData {
  constructor() {

  }
  public static mfilght: Flight[] = [
    {
      fullName:"Narodom Yatnimit",
      from:"Thailand",
      to:"Canada",
      type:"Oneway",
      adults:1,
      children:0,
      infants:0,
      departure:  new NgbDate(2022,3,3),
      arrival: new NgbDate(2022,3,11)
    },
    {
      fullName:"Beeba Company",
      from:"Thailand",
      to:"America",
      type:"Return",
      adults:90,
      children:0,
      infants:0,
      departure:  new NgbDate(2022,3,3),
      arrival: new NgbDate(2022,6,12)
    }
    ]
}
