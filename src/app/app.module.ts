import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {NgbCalendar , NgbCalendarHebrew , NgbDatepickerI18n , NgbDatepickerI18nHebrew} from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './app.component';
import { FormComponent } from './component/form/form.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldModule} from '@angular/material/form-field';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatSliderModule} from "@angular/material/slider";
import { MaterialModule } from './material.module'
import { MAT_DATE_LOCALE} from '@angular/material/core';
import {MAT_RADIO_DEFAULT_OPTIONS} from "@angular/material/radio";

@NgModule({
  declarations: [
    AppComponent,
    FormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    BrowserAnimationsModule,
    MatSliderModule,
    MaterialModule
  ],
  providers: [
    {provide: NgbCalendar, useClass: NgbCalendarHebrew},
    {provide: NgbDatepickerI18n, useClass: NgbDatepickerI18nHebrew},
    {provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: {appearance: 'fill'}}
    // {provide: MAT_RADIO_DEFAULT_OPTIONS, useValue: { color: 'accent' },}
],
  bootstrap: [AppComponent]
})
export class AppModule { }
