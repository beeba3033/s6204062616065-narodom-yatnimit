import { Component, OnInit ,Injectable } from '@angular/core';
import {Flight} from "./flight";
import {FormBuilder, FormControl, FormGroup , Validators ,  ValidatorFn , ValidationErrors ,AbstractControl} from "@angular/forms";
import {NgbDate, NgbCalendar, NgbDateParserFormatter ,   NgbDateStruct, NgbDatepickerI18n, NgbCalendarBuddhist} from '@ng-bootstrap/ng-bootstrap';
import localeThai from '@angular/common/locales/th';
import { getLocaleDayNames, FormStyle, TranslationWidth, getLocaleMonthNames, formatDate, registerLocaleData } from '@angular/common';
import {PageService} from "../../service/page.service";

@Injectable()
export class NgbDatepickerI18nBuddhist extends NgbDatepickerI18n {

  private _locale = 'th';
  private _weekdaysShort: readonly string[];
  private _monthsShort: readonly string[];
  private _monthsFull: readonly string[];

  constructor() {
    super();

    registerLocaleData(localeThai);

    const weekdaysStartingOnSunday = getLocaleDayNames(this._locale, FormStyle.Standalone, TranslationWidth.Short);
    this._weekdaysShort = weekdaysStartingOnSunday.map((day, index) => weekdaysStartingOnSunday[(index + 1) % 7]);

    this._monthsShort = getLocaleMonthNames(this._locale, FormStyle.Standalone, TranslationWidth.Abbreviated);
    this._monthsFull = getLocaleMonthNames(this._locale, FormStyle.Standalone, TranslationWidth.Wide);
  }

  getMonthShortName(month: number): string { return this._monthsShort[month - 1] || ''; }

  getMonthFullName(month: number): string { return this._monthsFull[month - 1] || ''; }

  getWeekdayLabel(weekday: number) {
    return this._weekdaysShort[weekday - 1] || '';
  }

  getDayAriaLabel(date: NgbDateStruct): string {
    const jsDate = new Date(date.year, date.month - 1, date.day);
    return formatDate(jsDate, 'fullDate', this._locale);
  }

  override getYearNumerals(year: number): string { return String(year); }
}
@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css'],
  providers: [
    { provide: NgbCalendar, useClass: NgbCalendarBuddhist },
    { provide: NgbDatepickerI18n, useClass: NgbDatepickerI18nBuddhist }
  ]
})
export class FormComponent implements OnInit {
  //Form---------------------------------------------------------------------
  flight:Flight ;
  flights: Array<Flight> = [];
  flightForm: FormGroup ;
  flightControl: FormControl = new FormControl() ;
  location = [
    {name:"America"},
    {name:"Canada"},
    {name:"Germany"},
    {name:"United Kingdom"},
    {name:"Italy"},
    {name:"Spain"},
    {name:"Poland"},
    {name:"Romania"},
    {name:"Netherlands"},
    {name:"Sweden"},
    {name:"Switzerland"},
    {name:"Norway"},
    {name:"Ireland"},
    {name:"Thailand"},
  ];
  constructor(private fb:FormBuilder,public calendar: NgbCalendar, public formatter: NgbDateParserFormatter ,private PageService:PageService) {
    this.flight = new Flight("",
      "","","",
      1,0,0,calendar.getToday(),calendar.getNext(calendar.getToday(),'d',1));
    this.flightForm = this.fb.group({
      fullName: ["",[Validators.required , Validators.maxLength(20) , Validators.pattern("^[a-zA-Z\\s]*$")]] ,
      from: ["",[Validators.required,Validators.minLength(2)]] ,
      to: ["",[Validators.required]] ,
      type: ["",[Validators.required]] ,
      adults: [1,[Validators.required,Validators.min(1)]] ,
      children: [0,[Validators.min(0)]] ,
      infants: [0,[Validators.min(0)]] ,
      departure: [calendar.getToday(),[Validators.required]],
      arrival: [calendar.getNext(calendar.getToday(), 'd', 1)]
    });
    this.fromDate = calendar.getToday();
    this.toDate = calendar.getNext(calendar.getToday(), 'd', 1);
    this.flightForm.value.departure = this.fromDate;
    this.flightForm.value.arrival = this.toDate
  }
  //------------------------------------------------------------------------
  ngOnInit(): void {
    this.flights = this.PageService.getFlight()
  }
  hoveredDate: NgbDate | null = null;
  fromDate: NgbDate | null;
  toDate: NgbDate | null;
  onDateSelection(date: NgbDate) {
    if (!this.flightForm.value.departure && !this.flightForm.value.arrival) {
      this.flightForm.value.departure = date;
    } else if (this.flightForm.value.departure && !this.flightForm.value.arrival && date && date.after(this.flightForm.value.departure)) {
      this.flightForm.value.arrival = date;
    } else {
      this.flightForm.value.arrival = null;
      this.flightForm.value.departure = date;
    }
  }
  isHovered(date: NgbDate) {
    return this.flightForm.value.departure && !this.flightForm.value.arrival && this.hoveredDate && date.after(this.flightForm.value.departure) &&
      date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) { return this.flightForm.value.arrival && date.after(this.flightForm.value.departure) && date.before(this.flightForm.value.arrival); }

  isRange(date: NgbDate) {
    return date.equals(this.flightForm.value.departure) || (this.flightForm.value.arrival && date.equals(this.flightForm.value.arrival)) || this.isInside(date) ||
      this.isHovered(date);
  }

  validateInput(currentValue: NgbDate | null, input: string): NgbDate | null {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  }

  changeDefaultName(name:string){
    this.flight.fullName = name ;
  }
  locationCheck(formgroup:FormGroup):boolean{
    if(formgroup.get("from")?.value.toString() !== formgroup.get("to")?.value.toString() || formgroup.get("from")?.value.toString() === "" || formgroup.get("to")?.value.toString() === ""){
      return false;
    }
    return true;
  }
  toggleElement():void {
    if(this.locationCheck(this.flightForm)){
      alert("ไม่สามารถเลือก สถานที่เดียวกันได้");
    }
    else{
      this.flight = new Flight(
        this.flightForm.value.fullName ,
        this.flightForm.value.from ,
        this.flightForm.value.to ,
        this.flightForm.value.type ,
        this.flightForm.value.adults ,
        this.flightForm.value.children ,
        this.flightForm.value.infants ,
        this.flightForm.value.departure ,
        this.flightForm.value.arrival
      );
      this.PageService.addFlight(this.flight);
    }
  }

}
